# Environmental and Social Governance (ESG) Investing Strategies

## Building Quantitative & Qualitative Portfolios with Artificial Intelligence, Machine Learning, and Statistics

### Project Created By:

* Louise-Espérance Honoré
* Matthew Hall
* Chenguang Yang

## Project Overview

### Abstract

Goes here...

### Summary of Results

Goes here at the end...

### Null Hypothesis

Using artificial intelligence and social / environmental factors, we can match market returns while creating portfolios with a positive environmental and social impact.

### Tools Used

Will keep adding...

### Objectives & Progress

#### Phase 1: Project Setup & Data Mining

- [ ] Project Overview, Thesis, and Objectives
- [ ] Data Mining Class Definitions & Scripts
- [ ] Select Stocks to Add to Each Asset Class
- [ ] Gather Data For Analysis on Server

#### Phase 2: Measure ESG Factors

- [ ] Define Scores for Social Impact Factor
- [ ] Measure Social Impact of Stock / Asset Class
- [ ] Define Scores for Environmental Impact Factor
- [ ] Measure Environmental Impact of Stock / Asset Class
- [ ] Public Sentiment Factor using NLP (Twitter API?)

#### Phase 3: Create Dataset for Analysis

- [ ] Include Stock Performance w/ All Factors for Monthly
- [ ] Verify No Look-Ahead Bias

#### Phase 4: Form Portfolios w/ Various Strategies

- [ ] Filter Out / Limit Stocks Based Upon ESG Factors
- [ ] Form Portfolios Using Multiple Strategies
  - [ ] Neural Network Regression Predictions
  - [ ] Machine Learning Predictions
  - [ ] Linear Regression Predictions
  - [ ] Tangent Portfolio


#### Phase 5: Benchmark and Compare Results

- [ ] Market Performance (S&P 500, NASDAQ Indicies)
- [ ] Moreira's Quant Strategies

